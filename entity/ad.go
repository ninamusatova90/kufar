package entity

import (
	"fmt"
	"time"

	"github.com/google/uuid"
)

type Ad struct {
	ID          int64     `json:"id"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
	UserID      uuid.UUID `json:"user_id"`
}

func (a *Ad) Validate() error {
	if a.Name == "" {
		return fmt.Errorf("ad name is empty")
	}

	return nil
}
