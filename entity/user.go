package entity

import (
	"fmt"
	"time"
	"unicode/utf8"

	"github.com/google/uuid"
)

const (
	minPasswordLen = 4
	minEmailLen    = 4
)

type User struct {
	ID        uuid.UUID `json:"id"`
	Name      string    `json:"name"`
	Email     string    `json:"email"`
	Password  string    `json:"password"`
	Phone     *string   `json:"phone"`
	CreatedAt time.Time `json:"created_at"`
}

type Session struct {
	ID        uuid.UUID `json:"id"`
	UserID    uuid.UUID `json:"user_id"`
	CreatedAt time.Time `json:"created_at"`
}

func (u *User) Validate() error {
	countEmail := utf8.RuneCountInString(u.Email)
	if countEmail < minEmailLen {
		return fmt.Errorf("short email")
	}
	// TODO replace to  regular expression длина должна быть не больше const

	countPassword := utf8.RuneCountInString(u.Password)
	if countPassword < minPasswordLen {
		return fmt.Errorf("short password")
	}

	return nil
}
