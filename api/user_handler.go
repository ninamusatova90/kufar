package api

import (
	"Kufar/entity"
	"Kufar/service"
	"context"
	"encoding/json"
	"net/http"
	"time"

	"github.com/google/uuid"
)

type UserHandler struct {
	service *service.UserService
}

func NewUserHandler(s *service.UserService) *UserHandler {
	return &UserHandler{
		service: s,
	}
}

type UserService interface {
	Create(ctx context.Context, user entity.User) (entity.User, error)
	Login(ctx context.Context, email, password string) (uuid.UUID, error)
	UserByID(ctx context.Context, id uuid.UUID) (entity.User, error)
	Users(ctx context.Context) ([]entity.User, error)
}

func (h *UserHandler) CreateUser(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	var user entity.User

	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		sendError(ctx, w, err)
		return
	}

	_, err = h.service.Create(ctx, user)
	if err != nil {
		sendError(ctx, w, err)
		return
	}
}

func (h *UserHandler) Login(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	var user entity.User

	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		sendError(ctx, w, err)
		return
	}

	sessionID, err := h.service.Login(ctx, user.Email, user.Password)
	if err != nil {
		sendError(ctx, w, err)
		return
	}

	const maxAgeSec = 3600

	cookie := http.Cookie{
		Name:     "session_id",
		Value:    sessionID.String(),
		Path:     "/",
		HttpOnly: true,
		MaxAge:   maxAgeSec,
		Expires:  time.Now().Add(time.Hour),
		Secure:   true,
	}

	http.SetCookie(w, &cookie)
}

func (h *UserHandler) UserByID(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	id, err := uuid.Parse(r.PathValue("id"))
	if err != nil {
		sendError(ctx, w, err)
		return
	}

	user, err := h.service.UserByID(ctx, id)
	if err != nil {
		sendError(ctx, w, err)
		return
	}

	err = sendJSON(w, user)
	if err != nil {
		sendError(ctx, w, err)
		return
	}
}

func (h *UserHandler) Users(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	users, err := h.service.Users(ctx)
	if err != nil {
		sendError(ctx, w, err)
		return
	}

	err = sendJSON(w, users)
	if err != nil {
		sendError(ctx, w, err)
		return
	}
}
