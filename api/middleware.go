package api

import (
	"Kufar/entity"
	"Kufar/storage"
	"context"
	"log/slog"
	"net/http"

	"github.com/google/uuid"
)

type Middleware struct {
	l    *slog.Logger
	repo *storage.UserRepository
}

func NewMiddleware(l *slog.Logger, ur *storage.UserRepository) *Middleware {
	return &Middleware{
		l:    l,
		repo: ur,
	}
}

type MiddlewareRepository interface {
	SessionByID(ctx context.Context, userID uuid.UUID) (uuid.UUID, error)
}

func (m *Middleware) Logging(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		l := m.l.With("request_id", uuid.NewString())
		ctx := context.WithValue(r.Context(), entity.CtxLogger{}, l)
		r = r.WithContext(ctx)
		l.Info("incoming API request", "method", r.Method, "request", r.RequestURI)
		next.ServeHTTP(w, r)
	})
}

func (m *Middleware) Sessions(next http.HandlerFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		cookie, err := r.Cookie("session_id")
		if err != nil {
			sendError(ctx, w, err)
			return
		}

		sessionID, err := uuid.Parse(cookie.Value)
		if err != nil {
			sendError(ctx, w, err)
			return
		}

		session, err := m.repo.SessionByID(ctx, sessionID)
		if err != nil {
			sendError(ctx, w, err)
			return
		}

		ctx = context.WithValue(ctx, entity.CtxUserID{}, session.UserID)
		r = r.WithContext(ctx)

		next.ServeHTTP(w, r)
	})
}
