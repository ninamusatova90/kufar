package api

import (
	"Kufar/entity"
	"context"
	"encoding/json"
	"log/slog"
	"net/http"
)

func sendError(ctx context.Context, w http.ResponseWriter, err error) {
	l, ok := ctx.Value(entity.CtxLogger{}).(*slog.Logger)
	if !ok {
		panic("no logger in context")
	}

	w.Header().Set("Content-Type", "application/json")
	_, _ = w.Write([]byte("problem in the program"))

	l.Error("sendError", "error", err)
}

func sendJSON(w http.ResponseWriter, body any) error {
	w.Header().Set("Content-Type", "application/json")

	err := json.NewEncoder(w).Encode(body)
	if err != nil {
		return err
	}
	return nil
}
