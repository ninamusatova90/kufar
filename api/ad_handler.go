package api

import (
	"Kufar/entity"
	"Kufar/service"
	"context"
	"encoding/json"
	"net/http"
	"strconv"
)

type AdHandler struct {
	service *service.AdService
}

func NewAdHandler(s *service.AdService) *AdHandler {
	return &AdHandler{service: s}
}

type AdService interface {
	CreateAd(ctx context.Context, ad entity.Ad) error
	AdByID(ctx context.Context, id int64) (entity.Ad, error)
}

func (a *AdHandler) CreateAd(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	var ad entity.Ad

	err := json.NewDecoder(r.Body).Decode(&ad)
	if err != nil {
		sendError(ctx, w, err)
		return
	}

	err = a.service.CreateAd(ctx, ad)
	if err != nil {
		sendError(ctx, w, err)
		return
	}
}

func (a *AdHandler) AdByID(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	idP := r.PathValue("id")

	id, err := strconv.ParseInt(idP, 10, 64)
	if err != nil {
		sendError(ctx, w, err)
		return
	}

	ad, err := a.service.AdByID(ctx, id)
	if err != nil {
		sendError(ctx, w, err)
		return
	}

	err = sendJSON(w, ad)
	if err != nil {
		sendError(ctx, w, err)
		return
	}
}
