package config

import (
	"github.com/ilyakaznacheev/cleanenv"
)

type Config struct {
	Port         string `yaml:"port"`
	Postgres     string `yaml:"postgres"`
	PostgresTest string `yaml:"postgres_test"`
}

func LoadConfig() (*Config, error) {
	var cfg Config

	err := cleanenv.ReadConfig("config.yaml", &cfg)
	if err != nil {
		return nil, err
	}

	return &cfg, err
}
