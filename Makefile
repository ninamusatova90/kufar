pg:
	docker run --name postgres -p 8015:5432 -e POSTGRES_PASSWORD=dev -e POSTGRES_DB=postgres -d --restart=always postgres:15.6

migration_up:
	goose -dir ./migrations postgres "postgres://postgres:dev@localhost:8015/postgres?sslmode=disable" up

migration_down:
	goose -dir ./migrations postgres "postgres://postgres:dev@localhost:8015/postgres?sslmode=disable" down


migration_reset:
	goose -dir ./migrations postgres "postgres://postgres:dev@localhost:8015/postgres?sslmode=disable" reset && \
	goose -dir ./migrations postgres "postgres://postgres:dev@localhost:8015/postgres?sslmode=disable" up

test:
	docker rm -f postgrestest
	docker run --name postgrestest -p 8091:5432 -e POSTGRES_PASSWORD=dev -e POSTGRES_DB=postgres -d postgres:15.6
	sleep 3
	goose -dir ./migrations postgres "postgres://postgres:dev@localhost:8091/postgres?sslmode=disable" up
	go test -v -race ./...

lint:
	golangci-lint  run --enable-all