package service

import (
	"context"
	"fmt"
	"time"

	"Kufar/entity"
	"Kufar/storage"

	"github.com/google/uuid"
)

type AdService struct {
	repo *storage.AdRepository
}

func NewAdService(r *storage.AdRepository) *AdService {
	return &AdService{
		repo: r,
	}
}

type AnnouncementRepository interface {
	CreateAd(ctx context.Context, ad entity.Ad) error
	AdByID(ctx context.Context, id int64) (entity.Ad, error)
}

func (a *AdService) CreateAd(ctx context.Context, ad entity.Ad) error {
	err := ad.Validate()
	if err != nil {
		return err
	}

	userID, ok := ctx.Value("user_id").(uuid.UUID)
	if !ok {
		return fmt.Errorf("not user id in context")
	}

	ad.UserID = userID
	ad.CreatedAt = time.Now()
	ad.UpdatedAt = ad.CreatedAt

	err = a.repo.CreateAd(ctx, ad)
	if err != nil {
		return err
	}
	return nil
}

func (a *AdService) AdByID(ctx context.Context, id int64) (entity.Ad, error) {
	ad, err := a.repo.AdByID(ctx, id)
	if err != nil {
		return entity.Ad{}, err
	}
	return ad, nil
}
