package service

import (
	"Kufar/entity"
	"Kufar/storage"
	"context"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
	"time"
)

type UserService struct {
	repo *storage.UserRepository
}

func NewUserService(ur *storage.UserRepository) *UserService {
	return &UserService{
		repo: ur,
	}
}

type UserRepository interface {
	CreateUser(ctx context.Context, user entity.User) error
	UserByEmail(ctx context.Context, email string) (entity.User, error)
	UserByID(ctx context.Context, id uuid.UUID) (entity.User, error)
	Users(ctx context.Context) ([]entity.User, error)
	SaveSession(ctx context.Context, userID uuid.UUID, sessionID uuid.UUID, createdAt time.Time) error
	SessionByID(ctx context.Context, userID uuid.UUID) (session entity.Session, err error)
}

func hashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return string(bytes), err
}

func checkPasswordHash(password, hash string) error {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err
}

func (us *UserService) Create(ctx context.Context, user entity.User) (entity.User, error) {
	err := user.Validate()
	if err != nil {
		return entity.User{}, err
	}

	user.ID = uuid.New()
	user.CreatedAt = time.Now()

	user.Password, err = hashPassword(user.Password)
	if err != nil {
		return entity.User{}, err
	}

	err = us.repo.CreateUser(ctx, user)
	if err != nil {
		return entity.User{}, err
	}

	user.Password = ""

	return user, nil
}

func (us *UserService) Login(ctx context.Context, email, password string) (uuid.UUID, error) {
	user, err := us.repo.UserByEmail(ctx, email)
	if err != nil {
		return uuid.UUID{}, err
	}

	err = checkPasswordHash(password, user.Password)
	if err != nil {
		return uuid.UUID{}, err
	}

	sessionID := uuid.New()

	err = us.repo.SaveSession(ctx, user.ID, sessionID, time.Now())
	if err != nil {
		return uuid.UUID{}, err
	}

	return sessionID, nil
}

func (us *UserService) UserByID(ctx context.Context, id uuid.UUID) (entity.User, error) {
	user, err := us.repo.UserByID(ctx, id)
	if err != nil {
		return entity.User{}, err
	}

	return user, nil
}

func (us *UserService) Users(ctx context.Context) ([]entity.User, error) {
	users, err := us.repo.Users(ctx)
	if err != nil {
		return nil, err
	}

	return users, nil
}
