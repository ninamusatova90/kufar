package main

import (
	"context"
	"fmt"
	"log"
	"log/slog"
	"net/http"
	"os"
	"time"

	"Kufar/api"
	"Kufar/config"
	"Kufar/service"
	"Kufar/storage"

	"github.com/jackc/pgx/v5"
)

func main() {
	cfg, err := config.LoadConfig()
	if err != nil {
		log.Fatal(err)
	}

	logger := slog.New(slog.NewJSONHandler(os.Stdout, nil))

	ctx := context.Background()

	conn, err := pgx.Connect(ctx, cfg.Postgres)
	if err != nil {
		logger.Error("unable to connect to database", "error", err)
		return
	}

	err = conn.Ping(ctx)
	if err != nil {
		logger.Error("ping db", "error", err)
		return
	}

	defer conn.Close(ctx)

	ur := storage.NewUserRepository(conn)
	ar := storage.NewAdRepository(conn)

	us := service.NewUserService(ur)
	as := service.NewAdService(ar)

	mw := api.NewMiddleware(logger, ur)
	uh := api.NewUserHandler(us)
	ah := api.NewAdHandler(as)

	r := http.NewServeMux()

	r.HandleFunc("POST /users", uh.CreateUser)
	r.Handle("GET /users/{id}", mw.Sessions(uh.UserByID))
	r.HandleFunc("GET /users", uh.Users)
	r.HandleFunc("POST /login", uh.Login)

	r.HandleFunc("POST /ads", ah.CreateAd)
	// получение 1 и списка

	server := &http.Server{
		Addr:              fmt.Sprintf(":%s", cfg.Port),
		Handler:           mw.Logging(r),
		ReadTimeout:       time.Second,
		WriteTimeout:      time.Second,
		ReadHeaderTimeout: time.Second,
	}
	err = server.ListenAndServe()
	if err != nil {
		logger.Error("listen and serve", "error", err)
		return
	}
}
