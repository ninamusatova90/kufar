package storage_test

import (
	"Kufar/entity"
	"Kufar/storage"
	"context"
	"os"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
	"github.com/stretchr/testify/require"
)

func DBConnection(t *testing.T) (context.Context, *pgx.Conn) {
	t.Helper()

	connString := os.Getenv("POSTGRES_CONN_STRING")
	if connString == "" {
		connString = "postgres://postgres:dev@localhost:8091/postgres?sslmode=disable"
	}

	ctx := context.Background()

	conn, err := pgx.Connect(ctx, connString)
	require.NoError(t, err)

	t.Cleanup(func() {
		err = conn.Close(ctx)
		require.NoError(t, err)
	})

	err = conn.Ping(ctx)
	require.NoError(t, err)

	return ctx, conn
}

func TestUserRepository_CreateUser(t *testing.T) {
	t.Parallel()

	ctx, conn := DBConnection(t)
	ur := storage.NewUserRepository(conn)
	phone := "1234"
	createdAt := time.Now().UTC().Round(time.Millisecond)
	user := entity.User{
		ID:        uuid.New(),
		Name:      "Nina",
		Email:     uuid.NewString(),
		Password:  uuid.NewString(),
		Phone:     &phone,
		CreatedAt: createdAt,
	}

	err := ur.CreateUser(ctx, user)
	if err != nil {
		require.NoError(t, err)
	}

	userByEmail, err := ur.UserByEmail(ctx, user.Email)
	require.NoError(t, err)
	require.Equal(t, user.Password, userByEmail.Password)
	user.Password = ""

	userDB, err := ur.UserByID(ctx, user.ID)
	require.NoError(t, err)
	require.Equal(t, user, userDB)
}

func TestUserRepository_UserByID_NotFound(t *testing.T) {
	t.Parallel()

	ctx, conn := DBConnection(t)
	ur := storage.NewUserRepository(conn)

	_, err := ur.UserByID(ctx, uuid.New())
	require.Error(t, err)
}
