package storage

import (
	"Kufar/entity"
	"context"

	"github.com/jackc/pgx/v5"
)

type AdRepository struct {
	db *pgx.Conn
}

func NewAdRepository(db *pgx.Conn) *AdRepository {
	return &AdRepository{
		db: db,
	}
}

func (a *AdRepository) CreateAd(ctx context.Context, ad entity.Ad) error {
	query := "INSERT INTO ads (name, description, created_at, updated_at, user_id) VALUES ($1, $2, $3, $4, $5)"

	_, err := a.db.Exec(ctx, query, ad.Name, ad.Description, ad.CreatedAt, ad.UpdatedAt, ad.UserID)
	if err != nil {
		return err
	}

	return nil
}

func (a *AdRepository) AdByID(ctx context.Context, id int64) (entity.Ad, error) {
	query := "SELECT id, name, description, created_at,updated_at, user_id FROM ads WHERE id = $1"

	var ad entity.Ad

	err := a.db.QueryRow(ctx, query, id).Scan(&ad.ID, &ad.Name, &ad.Description, &ad.CreatedAt, &ad.UpdatedAt, &ad.UserID)
	if err != nil {
		return entity.Ad{}, err
	}

	return ad, nil
}
