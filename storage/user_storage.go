package storage

import (
	"context"
	"time"

	"Kufar/entity"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

type UserRepository struct {
	db *pgx.Conn
}

func NewUserRepository(db *pgx.Conn) *UserRepository {
	return &UserRepository{
		db: db,
	}
}

func (r *UserRepository) CreateUser(ctx context.Context, u entity.User) error {
	query := "INSERT INTO users (id, name, email, password, phone, created_at) VALUES ( $1, $2, $3, $4, $5, $6)"

	_, err := r.db.Exec(ctx, query, u.ID, u.Name, u.Email, u.Password, u.Phone, u.CreatedAt)
	if err != nil {
		return err
	}

	return nil
}

func (r *UserRepository) UserByEmail(ctx context.Context, email string) (u entity.User, err error) {
	query := "SELECT id, name, email, password, phone, created_at FROM users WHERE email = $1"

	err = r.db.QueryRow(ctx, query, email).
		Scan(
			&u.ID,
			&u.Name,
			&u.Email,
			&u.Password,
			&u.Phone,
			&u.CreatedAt,
		)
	if err != nil {
		return entity.User{}, err
	}

	return u, nil
}

func (r *UserRepository) UserByID(ctx context.Context, id uuid.UUID) (u entity.User, err error) {
	query := "SELECT id, name, email, phone, created_at FROM users WHERE id = $1"

	err = r.db.QueryRow(ctx, query, id).Scan(&u.ID, &u.Name, &u.Email, &u.Phone, &u.CreatedAt)
	if err != nil {
		return entity.User{}, err
	}

	return u, nil
}

func (r *UserRepository) Users(ctx context.Context) (users []entity.User, err error) {
	query := "SELECT id, name, email, phone, created_at FROM users"

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	var user entity.User

	_, err = pgx.ForEachRow(rows, []any{&user.ID, &user.Name, &user.Email, &user.Phone, &user.CreatedAt}, func() error {
		users = append(users, user)

		return nil
	})
	if err != nil {
		return nil, err
	}

	return users, nil
}

func (r *UserRepository) SaveSession(ctx context.Context, userID, sessionID uuid.UUID, createdAt time.Time) error {
	query := "INSERT INTO sessions (id, user_id, created_at) VALUES ($1, $2, $3)"

	_, err := r.db.Exec(ctx, query, sessionID, userID, createdAt)
	if err != nil {
		return err
	}

	return nil
}

func (r *UserRepository) SessionByID(ctx context.Context, sessionID uuid.UUID) (session entity.Session, err error) {
	query := "SELECT id, user_id, created_at FROM sessions WHERE id = $1"

	err = r.db.QueryRow(ctx, query, sessionID).Scan(&session.ID, &session.UserID, &session.CreatedAt)
	if err != nil {
		return entity.Session{}, err
	}
	return session, nil
}
