-- +goose Up
CREATE TABLE users(
    id uuid PRIMARY KEY,
    name TEXT NOT NULL,
    email TEXT UNIQUE NOT NULL,
    password TEXT NOT NULL,
    phone TEXT,
    created_at timestamp NOT NULL
);

CREATE TABLE sessions(
    id uuid PRIMARY KEY,
    user_id uuid REFERENCES users (id) ON DELETE CASCADE NOT NULL,
    created_at timestamp NOT NULL
);

CREATE TABLE adverts(
    id BIGSERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    description TEXT  NOT NULL,
    created_at timestamp NOT NULL,
    updated_at timestamp NOT NULL,
    user_id uuid REFERENCES users (id) ON DELETE CASCADE NOT NULL
);

-- +goose Down
DROP TABLE users;
DROP TABLE sessions;
DROP TABLE adverts;


